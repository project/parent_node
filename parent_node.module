<?php

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_node_insert().
 */
function parent_node_node_insert($node) {
  parent_node_node_update($node);
}

/**
 * Implements hook_node_delete().
 */
function parent_node_node_delete($node) {
  $database = \Drupal::database();
  $query = $database->select('node__children_pages', 'nc')
    ->condition('nc.children_pages_target_id', $node->id())
    ->fields('nc', ['entity_id']);
  $result = $query->execute();
  if (!empty($result)) {
    // Remove the node from every child field where it is referenced.
    foreach ($result as $record) {
      $parent_node = Drupal::entityTypeManager()->getStorage('node')->load($record->entity_id);
      if ($parent_node->hasField('children_pages') && !$parent_node->get('children_pages')->isEmpty()) {
        $child_field = $parent_node->get('children_pages')->getValue();
        $index = array_search($node->id(), array_column($child_field, 'target_id'));
        $parent_node->get('children_pages')->removeItem($index);
        $parent_node->save();
      }
    }
  }
}

/**
 * Implements hook_node_update().
 */
function parent_node_node_update($node) {
  // Update current parent.
  if ($parents = $node->get('parent_page')->referencedEntities()) {
    foreach ($parents as $parent) {
      if (array_search(['target_id' => $node->id()], $parent->get('children_pages')->getValue()) === FALSE) {
        $parent->get('children_pages')->appendItem(['target_id' => $node->id()]);
        $parent->save();
      }
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function parent_node_node_presave($node) {
  // Update previous parent's children field if the current node has updated parents.
  if (!$node->isNew() && !$node->original->get('parent_page')->isEmpty()) {
    $original_parents = $node->original->get('parent_page')->referencedEntities();
    $new_parents = $node->get('parent_page')->referencedEntities();

    // Re-key the arrays by nid.
    $original_parents_ids = [];
    foreach ($original_parents as $key => $value) {
      $original_parents_ids[$value->id()] = $value;
    }

    $new_parents_ids = [];
    foreach ($new_parents as $key => $value) {
      $new_parents_ids[$value->id()] = $value;
    }

    // Calculate which nodes were removed.
    $removed_parents = array_diff_key($original_parents_ids, $new_parents_ids);

    // Remove the node as a child from the previous parent page.
    if (!empty($removed_parents)) {
      foreach ($removed_parents as $removed_parent) {
        $children_pages = $removed_parent->get('children_pages')->getValue();
        unset($children_pages[array_search(['target_id' => $node->id()], $children_pages)]);
        $removed_parent->set('children_pages', $children_pages);
        $removed_parent->save();
      }
    }
  }
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function parent_node_form_node_form_alter(&$form, $form_state) {
  // Add custom validate function.
  $form['#validate'][] = '_check_self_parent_reference';
}

/**
 * Custom validation function for node self referencing.
 */
function _check_self_parent_reference(&$form, $form_state) {
  if (!$form_state->getFormObject()->getEntity()->isNew()) {
    $current_node = $form_state->getFormObject()->getEntity();
    if ($form_state->getValue('parent_page')[0]['target_id'] == $current_node->id()) {
      $form_state->setErrorByName('parent_page', t('A page cannot be its own parent.'));
    }
  }
}

/**
 * Implements hook_entity_base_field_info().
 */
function parent_node_entity_base_field_info(\Drupal\Core\Entity\EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'node') {
    $fields['parent_page'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent page'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -10,
      ])
      ->setSettings([
        'target_type' => 'node',
        'handler' => 'default',
        'handler_settings' => [],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['children_pages'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Children pages'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setCardinality(-1)
      ->setDisplayOptions('view', [
        'type' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_labels',
        'weight' => -9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
}