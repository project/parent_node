<?php

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function parent_node_pathauto_token_info() {
  $info = [];

  $info['tokens']['node']['parent_page'] = [
    'name' => t('Parent page'),
    'description' => t('Parent page'),
    'type' => 'node',
  ];
  return $info;
}

/**
 * Implements hook_tokens().
 */
function parent_node_pathauto_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $replacements = array();
  if ($type == 'node'&& $node = $data['node']) {
    foreach ($tokens as $name => $original) {
      // Find the desired token by name
      switch ($name) {
        case 'parent_page':
          if (!$node->get('parent_page')->isEmpty()) {
            $replacements[$original] = $node->get('parent_page')->entity->getTitle();
          }
          break;
      }
    }
    if (!$node->get('parent_page')->isEmpty()) {
      if ($parent_tokens = $token_service->findWithPrefix($tokens, 'parent_page')) {
        $replacements += $token_service->generate('node', $parent_tokens, ['node' => $node->get('parent_page')->entity], $options, $bubbleable_metadata);
      }
    }
  }
  // Return the replacements.
  return $replacements;
}
